const express = require('express')
const { createWorkout, getSingleWorkout, getWorkout, deleteWorkout, updateWorkout } = require('../controller/workoutController')


const router = express.Router()

//GET
router.get('/', getWorkout)

//Single GET
router.get('/:id', getSingleWorkout)

//POST
router.post('/', createWorkout)

//DELETE
router.delete('/:id',deleteWorkout)

//UPDATE
router.patch('/:id',updateWorkout)


module.exports = router